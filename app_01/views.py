from django.shortcuts import render,redirect
from .forms import Schedule
from .models import Person
from django.contrib import messages
# Create your views here.

def index(request):
    return render(request, 'Story_03.html')
def skill(request):
    return render(request, 'Skill.html')
def form(request):
    if request.method == "POST":
        data = Schedule(request.POST or None)
        if data.is_valid():
            data.save()
            return redirect('show')
        else :
          messages.warning(request, "Oops, there's invalid! \n Please re-input peeps!")
    else :
        data = Schedule()
    return render(request, 'schedule.html', {'form': data})
def show(request):
    data = Person.objects.all()
    return render(request, 'mySchedule.html', {'data' : data})
def remove_activity(request, id):
    remove = Person.objects.get(id=id)
    remove.delete()
    arguments = {
        'data' : Person.objects.all(),
    }
    return render(request,'mySchedule.html',arguments)

  