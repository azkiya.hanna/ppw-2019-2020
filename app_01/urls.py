from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('skill',views.skill,name='skill'),
    path('schedule',views.form,name='schedule'),
    path('show',views.show,name='show'),
    path('mySchedule',views.remove_activity,name='mySchedule'),
    path('delete /<id>',views.remove_activity,name='delete')
]