from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Person(models.Model):
    hari = models.CharField(max_length=10)
    tanggal = models.DateField()
    waktu = models.TimeField()
    aktivitas = models.CharField(max_length=25)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=30)

    # def __str__(self):
    #     return "{}.{}".format(self.id,self.hari)