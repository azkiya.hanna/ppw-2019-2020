from django import forms
from django.db import models
from .models import Person

class Schedule(forms.ModelForm):
    class Meta :
        model = Person
        fields = {"aktivitas","hari","tempat","kategori","tanggal","waktu"}
        widgets = {
            'hari' : forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'Type the day',
                }
            ),
            'aktivitas' : forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'Type the activity',
                }
            ),
            'tempat' : forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'Type the place',
                }
            ),
            'kategori' : forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'Type the category',
                }
            ),
            'tanggal' : forms.DateInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'YYYY-MM-DD',
                }
            ),
            'waktu' : forms.TimeInput(
                 attrs = {
                    'class': 'form-control',
                    'placeholder': 'Hour:Minute',
                }
            ),
        }
        labels = {
            'hari' : 'Day ',
            'aktivitas' : 'Activity ',
            'tempat' : 'Place  ',
            'kategori' : 'Category ',
            'tanggal' : 'Date ',
            'waktu' : 'Time ',
        }